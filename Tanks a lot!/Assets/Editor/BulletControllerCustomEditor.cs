﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(AmmoController))]
public class BulletControllerCustomEditor :Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        var bulletController = target as AmmoController;

        var useExplosion = serializedObject.FindProperty("UseExplosionForce");
        EditorGUILayout.PropertyField(useExplosion);
        
        using (var group = new EditorGUILayout.FadeGroupScope(Convert.ToSingle(useExplosion.boolValue)))
        {
            if (group.visible == true)
            {
                EditorGUI.indentLevel++;
                var explosionRadius = serializedObject.FindProperty("ExplosionRadius");
                EditorGUILayout.PropertyField(explosionRadius);

                EditorGUI.indentLevel--;
            }
        }
        serializedObject.ApplyModifiedProperties();
    }
}
