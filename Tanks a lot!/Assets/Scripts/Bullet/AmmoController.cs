﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmmoController : MonoBehaviour
{
    [HideInInspector]
    public bool UseExplosionForce;
    [HideInInspector]
    public float ExplosionRadius;

    private int _enemyLayer;

    public int EnemyLayer { get { return _enemyLayer; } set { _enemyLayer = value; } }
    public string ShooterName { get; set; }
    public ObjectPool Pool { get; set; }
}
