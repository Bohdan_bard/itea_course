﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallBehaviour : MonoBehaviour
{
    [SerializeField]
    private int _damage = default;
    [SerializeField]
    private GameObject _explosionPrefab = null;
    [SerializeField]
    private float _speed = default;

    private AmmoController _ammoController;
    private Rigidbody _rigidbody;

    public AmmoController AmmoController { set { _ammoController = value; } }

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();  
    }

    private void OnEnable()
    {
        transform.localPosition = Vector3.zero;
    }

    private void Update()
    {
        _rigidbody.velocity = transform.parent.forward * _speed;
    }

    private void OnTriggerEnter(Collider other)
    {
            if (other.gameObject.layer == _ammoController.EnemyLayer)
            {
                var tankHealth = other.GetComponent<TankHealth>();
                if (tankHealth != null)
                {
                    tankHealth.DecreaseHealth(_damage, _ammoController.ShooterName);
                }
            }
       
        InitExplosion.Explode(_ammoController.Pool, _explosionPrefab, transform.position);

        gameObject.SetActive(false);
    }
}
