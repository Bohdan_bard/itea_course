﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    [SerializeField]
    private float _lifeTime = 2f;  
    [SerializeField]
    private GameObject[] _balls = null;

    private AmmoController _ammoController;
    private float _lifeTimeTimer = 0;
    private WaitForSeconds _waitForBallSpawn = new WaitForSeconds(0.1f);

    private void OnEnable()
    {
        StartCoroutine(BallsSpawn());
        _lifeTimeTimer = 0f;
    }

    void Start()
    {
        _ammoController = GetComponent<AmmoController>();
        for (int i = 0; i < _balls.Length; i++)
        {
            _balls[i].GetComponent<BallBehaviour>().AmmoController = _ammoController;
        }
            StartCoroutine(BallsSpawn());
    }
    
    void Update()
    {
        _lifeTimeTimer += Time.deltaTime;

        if (gameObject.activeInHierarchy && _lifeTimeTimer > _lifeTime)
        {
            Deactivate();
        }
    }

    private IEnumerator BallsSpawn()
    {
        for (int i = 0; i < _balls.Length; i++)
        {
            _balls[i].SetActive(true);
            yield return _waitForBallSpawn;
        }
    }

    public void Deactivate()
    {
        for (int i = 0; i < _balls.Length; i++)
        {
            _balls[i].SetActive(false);
        }
     transform.parent.gameObject.SetActive(false);
    }
}
