﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    [SerializeField]
    private float _speed = 10f;
    [SerializeField]
    private float _lifeTime = 2f;
    [SerializeField]
    private int _damage = 10;
    [SerializeField]
    private GameObject _explosionPrefab = null;

    private AmmoController _ammoController;
    private float _lifeTimeTimer = 0;
    private bool _isMoovable = true;
    private Rigidbody _rigidbody;

    private void    OnEnable()
    {
        _isMoovable = true;
        _lifeTimeTimer = 0f;
    }
 
    void Start()
    {
        _ammoController = GetComponent<AmmoController>();
        _rigidbody = GetComponent<Rigidbody>();
    }

    void Update()
    {
        _lifeTimeTimer += Time.deltaTime;
        if (_isMoovable)
        {
            _rigidbody.velocity = transform.parent.forward * _speed;
        }

        if (gameObject.activeInHierarchy && _lifeTimeTimer > _lifeTime)
        {
            Deactivate();
        }
    }

    public void Deactivate()
    {
        transform.parent.gameObject.SetActive(false);
        transform.position = Vector3.zero;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (_ammoController.UseExplosionForce)
        {
            AddExplosion();
        }
        else
        {
            if (other.gameObject.layer == _ammoController.EnemyLayer)
            {
                var tankHealth = other.GetComponent<TankHealth>();
                if (tankHealth != null)
                {
                    tankHealth.DecreaseHealth(_damage, _ammoController.ShooterName);
                }

            }
        }
        InitExplosion.Explode(_ammoController.Pool, _explosionPrefab, transform.position);

        Deactivate();
    }

    private void AddExplosion()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, _ammoController.ExplosionRadius, (int)Mathf.Pow(2, _ammoController.EnemyLayer));

        for (int i = 0; i < colliders.Length; i++)
        {

            Rigidbody targetRigidbody = colliders[i].GetComponent<Rigidbody>();

            if (!targetRigidbody)
                continue;

            TankHealth targetHealth = targetRigidbody.GetComponent<TankHealth>();

            if (!targetHealth)
                continue;

            float damage = CalculateDamage(targetRigidbody.position);

            targetHealth.DecreaseHealth((int)damage, _ammoController.ShooterName);
        }
    }


    private float CalculateDamage(Vector3 targetPosition)
    {

        Vector3 explosionToTarget = targetPosition - transform.position;

        float explosionDistance = explosionToTarget.magnitude;

        float relativeDistance = (_ammoController.ExplosionRadius - explosionDistance) / _ammoController.ExplosionRadius;

        float damage = relativeDistance * _damage;

        damage = Mathf.Max(0, damage);

        return damage;
    }

    private void OnDisable()
    {
        _isMoovable = false;
    }
}
