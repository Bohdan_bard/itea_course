﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Deactivator : MonoBehaviour
{
    private void OnEnable()
    {
        Invoke("Deactivate", 2f);
    }

    private void Deactivate()
    {
        gameObject.SetActive(false);
    }
}
