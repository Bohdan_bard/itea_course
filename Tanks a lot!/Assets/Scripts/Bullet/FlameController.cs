﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlameController : MonoBehaviour
{
    [SerializeField]
    private float _lifeTime = 2f;
    [SerializeField]
    private float _distanceOfFlame = default;
    [SerializeField]
    private int _damage = default;
    [SerializeField]
    private Transform[] _flameDirections = null;

    private AmmoController _ammoController;
    private WaitForSeconds _waitForSec = new WaitForSeconds(0.3f);
    private float _lifeTimeTimer = 0;
    private int _enemyLayer;
    private bool _isEnable=false;

    private void Awake()
    {
        _ammoController = GetComponent<AmmoController>();
      
    }

    private void OnEnable()
    {
        _lifeTimeTimer = 0f;
        _isEnable = true;
    }

    void Start()
    {
        _isEnable = true;
        _enemyLayer = _ammoController.EnemyLayer;
      
        _lifeTimeTimer = 0f;
    }

    private void Update()
    {
        if (_isEnable)
        {
            StartCoroutine(Fire());
            _isEnable = false;
        }
        _lifeTimeTimer += Time.deltaTime;

        if (gameObject.activeInHierarchy && _lifeTimeTimer > _lifeTime)
        {
            Deactivate();
        }
    }

    private IEnumerator Fire()
    {
        int bitEnemyLayer = 1 << _enemyLayer;
        for (int i = 1; i <= 3; i++)
        {
            for (int j = 0; j < _flameDirections.Length; j++)
            {
                RaycastHit hit;
                
                if (Physics.Raycast(transform.position, _flameDirections[j].TransformDirection(Vector3.forward), out hit, _distanceOfFlame, bitEnemyLayer))
                {
                    var tankHealth = hit.collider.GetComponent<TankHealth>();
                    if (tankHealth)
                    {
                        tankHealth.DecreaseHealth((int)_damage, _ammoController.ShooterName);
                    }
                }
            }
            yield return _waitForSec;
        }
    }

    public void Deactivate()
    {   
        transform.parent.gameObject.SetActive(false);
    }

    
}
