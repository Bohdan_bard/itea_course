﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public enum FSMStateType
{
IDLE,
ATTACK,
RETREAT
}

namespace AbstractState
{
    public abstract class AbstractFSMState : ScriptableObject
    {
        protected AudioSource _audioSource;
        protected BotController _bot;
        protected FiniteStateMachine _fsm;
        protected NavMeshAgent _navMeshAgent;

        public FSMStateType StateType { get; protected set; }

        public virtual void OnEnable()
        {
  
        }

        public virtual bool EnterState()
        {
            return true;
        }

        public abstract void UpdateState();

        public virtual bool ExitState()
        {
            return true;
        }

        public virtual void SetBotController(BotController bot)
        {
            _bot = bot;
        }

        public virtual void SetFiniteStateMachine(FiniteStateMachine fms)
        {
            _fsm = fms;
        }

        public virtual void SetNavMeshAgent(NavMeshAgent navMeshAgent)
        {
            _navMeshAgent = navMeshAgent;
        }

        public virtual void SetAudioSource(AudioSource audioSource)
        {
            _audioSource = audioSource;
        }
    }
}
