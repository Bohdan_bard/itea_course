﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AbstractState;

public class CreateScriptableObjectInstance : MonoBehaviour
{
    [SerializeField]
    private AbstractFSMState _idleState = null;
    [SerializeField]
    private AbstractFSMState _attackState = null;
    [SerializeField]
    private AbstractFSMState _retreatState = null;

    public AbstractFSMState ReturnIdleState()
    {
        return Instantiate(_idleState);
    }

    public AbstractFSMState ReturnAttackState()
    {
        return Instantiate(_attackState);
    }

    public AbstractFSMState ReturnRetreatState()
    {
        return Instantiate(_retreatState);
    }
}
