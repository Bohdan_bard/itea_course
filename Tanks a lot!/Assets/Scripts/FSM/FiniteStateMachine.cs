﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using AbstractState;


public class FiniteStateMachine : MonoBehaviour
{
    [SerializeField]
    private int _minLevelHealth = default;

    private AbstractFSMState _startingState = null;  
    private List<AbstractFSMState> _validStates = null;
    private Dictionary<FSMStateType, AbstractFSMState> _fsmStates;
    private AbstractFSMState _currentState;
    private BotController _bot;
    private AbstractFSMState _nextState;
    private CreateScriptableObjectInstance _createScriptableObjectInstance;
    private AudioSource _audioSource;
    private NavMeshAgent _agent;

    public int MinLevelHealth { get { return _minLevelHealth; } }

    private void Awake()
    {
        _currentState = null;

        _bot = GetComponent<BotController>();
        _agent = _bot.Agent;
        _audioSource = _bot.GetComponent<AudioSource>();
        _fsmStates = new Dictionary<FSMStateType, AbstractFSMState>();

        _validStates = new List<AbstractFSMState>();
        _createScriptableObjectInstance = GetComponent<CreateScriptableObjectInstance>();
        _validStates.Add(_createScriptableObjectInstance.ReturnIdleState());
        _validStates.Add(_createScriptableObjectInstance.ReturnAttackState());
        _validStates.Add(_createScriptableObjectInstance.ReturnRetreatState());
        _startingState = _validStates[0];
    }

    private void Start()
    {
        foreach (AbstractFSMState state in _validStates)
        {
            state.SetBotController(_bot);
            _fsmStates.Add(state.StateType, state);
            state.SetFiniteStateMachine(this);
            state.SetNavMeshAgent(_bot.Agent);
            state.SetAudioSource(_audioSource);
        }

        if (_startingState != null)
        {
            EnterState(_startingState);
        }
    }

    public void Update()
    {
        if (_currentState != null)
        {
            _currentState = _nextState;
          
            _currentState.UpdateState();
        }
    }

    public void EnterState(AbstractFSMState nextState)
    {
        if (nextState == null)
        { 
            return;
        }

        if (_currentState != null)
        {  
            _currentState.ExitState();
        }

        _currentState = nextState;
       
        _currentState.EnterState();
        _nextState = nextState;
    }

    public void EnterState(FSMStateType stateType)
    {
        if (_fsmStates.ContainsKey(stateType))
        {
            AbstractFSMState nextState = _fsmStates[stateType];

            _currentState.ExitState();

            EnterState(nextState);
        }
    }
}
