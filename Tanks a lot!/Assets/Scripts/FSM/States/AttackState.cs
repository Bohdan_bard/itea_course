﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AbstractState;

[CreateAssetMenu(fileName = "AttackState", menuName = "FSM/States/Attack", order = 2)]
public class AttackState : AbstractFSMState
{
    [SerializeField]
    private AudioClip _moovingClip = null;

    public override void OnEnable()
    {
        base.OnEnable();
        StateType = FSMStateType.ATTACK;
    }

    public override bool EnterState()
    {
        base.EnterState();
        _navMeshAgent.isStopped = false;
        _audioSource.clip = _moovingClip;
        _audioSource.Play();
        _navMeshAgent.SetDestination(_bot.Target.transform.position);
        return true;
    }

    public override void UpdateState()
    {
        if (_bot.Health <= _fsm.MinLevelHealth || _bot.Ammo == 0)
        {
            _fsm.EnterState(FSMStateType.RETREAT);
        }

        else if (_bot.Target == null || !_bot.Target.gameObject.activeInHierarchy)
        {
            _fsm.EnterState(FSMStateType.IDLE);
        }
        else
        {
            _navMeshAgent.SetDestination(_bot.Target.transform.position);
        }
        
    }

    public override bool ExitState()
    {
        base.ExitState();
        return true;
    }
}
