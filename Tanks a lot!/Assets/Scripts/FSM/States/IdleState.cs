﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AbstractState;

[CreateAssetMenu(fileName = "IdleState", menuName ="FSM/States/Idle", order = 1)]
public class IdleState : AbstractFSMState
{
    [SerializeField]
    private AudioClip _idleClip = null;

    public override void OnEnable()
    { 
        base.OnEnable();
        StateType = FSMStateType.IDLE;
    }

    public override bool EnterState()
    {
       base.EnterState();
        _navMeshAgent.isStopped = true;
        _audioSource.clip = _idleClip;
        _audioSource.Play();
        return true;
    }

    public override void UpdateState()
    {
        if (_bot.Target != null)
        {
            if (_bot.Target.gameObject.activeInHierarchy)
            {
                _fsm.EnterState(FSMStateType.ATTACK);
            }
        }
    }

    public override bool ExitState()
    {
       base.ExitState();
        return true;
    }
}
