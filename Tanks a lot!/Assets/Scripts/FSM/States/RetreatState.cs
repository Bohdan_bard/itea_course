﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AbstractState;

[CreateAssetMenu(fileName = "RetreatState", menuName = "FSM/States/Retreat", order = 3)]
public class RetreatState : AbstractFSMState
{
    public override void OnEnable()
    {
        base.OnEnable();
        StateType = FSMStateType.RETREAT;
    }

    public override bool EnterState()
    {
        base.EnterState();
        if (_navMeshAgent.gameObject.activeInHierarchy)
        {
            _navMeshAgent.isStopped = false;
            _navMeshAgent.SetDestination(_bot.StartPosition);
        }
        return true;
    }

    public override void UpdateState()
    {
        if (_bot.Health > _fsm.MinLevelHealth && _bot.Ammo > 0)
        {
            if (_bot.Target != null && _bot.Target.gameObject.activeInHierarchy)
            {
                _fsm.EnterState(FSMStateType.ATTACK);
            }
            else
            {
                _fsm.EnterState(FSMStateType.IDLE);
            }
        }
    }

    public override bool ExitState()
    {
        base.ExitState();
        return true;
    }
}
