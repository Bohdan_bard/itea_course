﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Audio;

public class VolumeSetter : MonoBehaviour
{
    [SerializeField]
    private AudioMixer _sfxMixer= null;
    [SerializeField]
    private AudioMixer _musicMixer = null;
    [SerializeField]
    private Slider _sfxSlider = null;
    [SerializeField]
    private Slider _musicSlider = null;
   
    void Start()
    {
        var sfxVolume = PlayerPrefs.GetFloat("SFXVolume", 0);
        var musicVolume = PlayerPrefs.GetFloat("MusicVolume", 0);
        _sfxMixer.SetFloat("sfxVolume",sfxVolume );
        _musicMixer.SetFloat("musicVolume",musicVolume );
        _sfxSlider.value = sfxVolume;
        _musicSlider.value = musicVolume;
        _sfxSlider.onValueChanged.AddListener(SetSFXVolume);
        _musicSlider.onValueChanged.AddListener(SetMusicVolume);
    }

    private void SetMusicVolume(float value)
    {
        _musicMixer.SetFloat("musicVolume", value);
        PlayerPrefs.SetFloat("MusicVolume", value);
    }

    private void SetSFXVolume(float value)
    {
        _sfxMixer.SetFloat("sfxVolume", value);
        PlayerPrefs.SetFloat("SFXVolume", value);
    }
}
