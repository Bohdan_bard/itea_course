﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotNames : MonoBehaviour
{
    public static string[] AlliesNames = { "GoodBot", "NiceBot"};

    public static string[] EnemiesNames = { "BadBot", "EvilBot", "UglyBot"};
}
