﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InitExplosion : MonoBehaviour
{
   
   
   public static void Explode(ObjectPool pool, GameObject explosionPrefab, Vector3 initPosition)
    {
        var explosion = pool.ReturnObject(explosionPrefab.layer);
        if (explosion == null)
        {
            explosion = Instantiate(explosionPrefab, initPosition, explosionPrefab.transform.rotation, pool.transform);
            pool.AddToList(explosion, explosionPrefab.layer);

        }
        else
        {
            explosion.transform.position = initPosition;

            explosion.SetActive(true);
        }
    }
}
