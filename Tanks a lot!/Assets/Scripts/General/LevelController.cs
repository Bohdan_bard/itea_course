﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{
    public event SendInfoAction SendAllyPoints;
    public event SendInfoAction SendEnemyPoints;

    public  const int AlliesLayer = 8;
    public const int EnemyLayer = 11;
    private const int MaxAllies = 3;
    private const int MaxEnemies = 3;

    [SerializeField] private float _roundTime = 120; 
    [SerializeField] private GameObject _hudScreen = null;
    [SerializeField] private GameObject _gameOverScreen = null;
    [SerializeField] private Button _restartButton = null;
    [SerializeField] private Button _exitButton = null;
    [SerializeField] private TextMeshProUGUI _gameOverText = null;

    private TankCreator _tankCreator;
    private bool _isGameOn = false;
    private List<GameObject> _activeAllies;
    private List<GameObject> _activeEnemies;
    private List<GameObject> _inactiveAllies;
    private List<GameObject> _inactiveEnemies;
    private int _allyPoints = 0;
    private int _enemyPoints = 0;
    private float _timer = 0;
    private AddItem _addKillInfoItem;

    public bool IsGameOn { get { return _isGameOn; } }
    public float RoundTime { get { return _roundTime; } }

    private void Start()
    {
        _addKillInfoItem = GetComponent<AddItem>();
        _restartButton.onClick.AddListener(RestartGame);
        _exitButton.onClick.AddListener(Exit);
        _activeAllies = new List<GameObject>();
        _activeEnemies = new List<GameObject>();
        _inactiveAllies = new List<GameObject>();
        _inactiveEnemies = new List<GameObject>();
        _tankCreator = GetComponent<TankCreator>();
        StartLevel();
    }

    private void Update()
    {
        if (_isGameOn)
        {
            _timer += Time.deltaTime;
            if (_timer >= _roundTime)
            {
                gameObject.SetActive(false);
                _isGameOn = false;
                Debug.Log("in update");
                GameOver();
            }
        }
    }

    private void StartLevel()
    {
        _tankCreator.CreateAllyTeam();
        _tankCreator.CreateEnemyTeam();
        _activeAllies = _tankCreator.ActiveAllies;
        _activeEnemies = _tankCreator.ActiveEnemies;
        for (int i = 0; i < _activeAllies.Count; i++)
        {
          if (i == 0)
            {
                var playerController = _activeAllies[i].GetComponent<PlayerController>();
                playerController.RemoveFromActiveListEvent += RemoveFromActiveList;
                playerController.AddToActiveListEvent += AddToActiveList;
                playerController.SendNamesEvent += _addKillInfoItem.AddItemToContent;
            }
            else
            {
                var botController = _activeAllies[i].GetComponent<BotController>();
                botController.RemoveFromActiveListEvent += RemoveFromActiveList;
                botController.AddToActiveListEvent += AddToActiveList;
                botController.SendNamesEvent += _addKillInfoItem.AddItemToContent;
            }

            var    enemyBotController = _activeEnemies[i].GetComponent<BotController>();
            enemyBotController.RemoveFromActiveListEvent += RemoveFromActiveList;
            enemyBotController.AddToActiveListEvent += AddToActiveList;
            enemyBotController.SendNamesEvent += _addKillInfoItem.AddItemToContent;
        }
        _isGameOn = true;
    }

    

    private void Exit()
    {
        SceneManager.LoadScene("MainMenu");
    }   

    private void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void GameOver()
    {
        _gameOverScreen.SetActive(true);
        _hudScreen.SetActive(false);
     
        if (_allyPoints > _enemyPoints)
        {
            var earnedPoints = _allyPoints - _enemyPoints;
            var points = PlayerPrefs.GetInt("Points", 19);
            PlayerPrefs.SetInt("Points", points + earnedPoints);
            _gameOverText.text = $"Ally win! You earned {earnedPoints} points";
        }
        else if (_allyPoints < _enemyPoints)
        {
            _gameOverText.text = "You lose!";
        }
        else
        {
            _gameOverText.text = "Draw!";
        }
    }

    public void RemoveFromActiveList(GameObject tank)
    {
        var botController = tank.GetComponent<BotController>();
        if (botController == null)
        {
            _activeAllies.Remove(tank);
            _inactiveAllies.Add(tank);
            _enemyPoints++;
            if (SendEnemyPoints != null)
            {
                SendEnemyPoints(_enemyPoints);
            }
        }
        else
        {
            if (botController.IsEnemy == true)
            {
                _activeEnemies.Remove(tank);
                _inactiveEnemies.Add(tank);
                _allyPoints++;
                if (SendAllyPoints != null)
                {
                    SendAllyPoints(_allyPoints);
                }         
            }
            else
            {
                _activeAllies.Remove(tank);
                _inactiveAllies.Add(tank);
                _enemyPoints++;
                if (SendEnemyPoints != null)
                {
                    SendEnemyPoints(_enemyPoints);
                }   
            }
        }
    }

    private void AddToActiveList(GameObject tank)
    {
        var botController = tank.GetComponent<BotController>();
        if (botController == null)
        {
            _inactiveAllies.Remove(tank);
            _activeAllies.Add(tank);
        }
        else
        {
            if (botController.IsEnemy == true)
            {
                _inactiveEnemies.Remove(tank);
                _activeEnemies.Add(tank);
            }
            else
            {
                _inactiveAllies.Remove(tank);
                _activeAllies.Add(tank);
            }
        }
    }

    public Transform ReturnTarget(bool isEnemy)
    {       

        if (isEnemy)
        {
            if (_activeAllies.Count > 0)
            {  
                var randomNumber = (int)Random.Range(0, _activeAllies.Count - 1);         
                return _activeAllies[randomNumber].transform;
            }
            else
            {
                return null;
            }
        }
        else
        {
            if (_activeEnemies.Count >0)
            {  
                var randomNumber = (int)Random.Range(0, _activeEnemies.Count - 1);
                return _activeEnemies[(int)Random.Range(0, _activeEnemies.Count-1)].transform;
            }
            else
            {
                return null;
            }
        }
    }
}
