﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TankCreator : MonoBehaviour
{
    public const int AlliesLayer = 8;
    public const int EnemyLayer = 11;

    private const int MaxAllies = 3;
    private const int MaxEnemies = 3;

    [SerializeField] private GameObject _playerPrefab = null;
    [SerializeField] private GameObject _botPrefab = null;
    [SerializeField] private Transform[] _alliesSpawns = null;
    [SerializeField] private Transform[] _enemySpawns = null;
    [SerializeField] private Joystick _movementJoystick = null;
    [SerializeField] private Joystick _shootingJoystick = null;
    [SerializeField] private Joystick _ultimateJoystick = null;
    [SerializeField] private ObjectPool _pool = null;
    [SerializeField] private Transform _camera = null;
    [SerializeField] private Material _allyMaterial = default;
    [SerializeField] private Material _enemyMaterial = default;
    [SerializeField] private LayerMask _allyLayer = default;
    [SerializeField] private LayerMask _enemyLayer = default;
    [SerializeField] private Slider _reloadSlider = null;
    [SerializeField] private GameObject[] _playerChassies = null;
    [SerializeField] private GameObject[] _playerTowers = null;
    [SerializeField] private GameObject[] _botChassies = null;
    [SerializeField] private GameObject[] _botTowers = null;

    private List<GameObject> _activeAllies;
    private List<GameObject> _activeEnemies;
    private LevelController _levelController;
    

    public List<GameObject> ActiveAllies { get { return _activeAllies; } }
    public List<GameObject> ActiveEnemies { get { return _activeEnemies; } }

    void Awake()
    {
        _activeAllies = new List<GameObject>();
        _activeEnemies = new List<GameObject>();
        _levelController = GetComponent<LevelController>();
       
      
    }

  
    public void CreateAllyTeam()
    {
        for (int i = 0; i < MaxAllies; i++)
        {
            if (i == 0)
            {

                _activeAllies.Add(Instantiate(_playerPrefab, _alliesSpawns[i].position, _playerPrefab.transform.rotation));

                int towerIndex = PlayerPrefs.GetInt("TowerType", 0); 
             
                var tower = Instantiate(_playerTowers[towerIndex], 
                                        _activeAllies[i].transform.position+_playerTowers[towerIndex].transform.position, 
                                        _playerTowers[towerIndex].transform.rotation);
                tower.transform.SetParent(_activeAllies[i].transform);
                tower.transform.SetAsFirstSibling();
                var chassiesIndex = PlayerPrefs.GetInt("ChassiesType", 0);
                var chassie = Instantiate(_playerChassies[chassiesIndex], _activeAllies[i].transform.position, _playerChassies[chassiesIndex].transform.rotation);
                chassie.transform.SetParent(_activeAllies[i].transform);
                chassie.transform.SetAsFirstSibling();
                var playerController = _activeAllies[i].GetComponent<PlayerController>();
              
                
                playerController.MovementJoystick = _movementJoystick;
                playerController.ShootingJoystick = _shootingJoystick;
                playerController.Pool = _pool;
                playerController.Camera = _camera;
                playerController.UltimateJoystick = _ultimateJoystick;
                playerController.Name = "Player";
                playerController.ReloadSlider = _reloadSlider;
            }
            else
            {
                _activeAllies.Add(Instantiate(_botPrefab, _alliesSpawns[i].position, _botPrefab.transform.rotation));
                CreateBotParts(i, _activeAllies[i]);
                var botController = _activeAllies[i].GetComponent<BotController>();
                botController.Pool = _pool;
                botController.Name = BotNames.AlliesNames[i - 1];
                botController.LevelController = _levelController;
                botController.IsEnemy = false;
                botController.Material = _allyMaterial;
                botController.EnemyLayer = _enemyLayer;
                botController.BulletReactLayer = EnemyLayer;
                
                botController.TeamLayer = AlliesLayer;
               
            }
        }
    }

    public void CreateEnemyTeam()
    {
        for (int i = 0; i < MaxEnemies; i++)
        {
            _activeEnemies.Add(Instantiate(_botPrefab, _enemySpawns[i].position, _botPrefab.transform.rotation));
            CreateBotParts(i, _activeEnemies[i]);
            var enemyBotController = _activeEnemies[i].GetComponent<BotController>();
            enemyBotController.Pool = _pool;
            enemyBotController.Name = BotNames.EnemiesNames[i];
            enemyBotController.LevelController = _levelController;
            enemyBotController.IsEnemy = true;
            enemyBotController.Material = _enemyMaterial;
            enemyBotController.EnemyLayer = _allyLayer;
            enemyBotController.BulletReactLayer = AlliesLayer;
            
            enemyBotController.TeamLayer = EnemyLayer;
            
        }
    }

    private void CreateBotParts(int index, GameObject currentBot)
    {
        int randomIndex = Random.Range(0, _botTowers.Length);

        var tower = Instantiate(_botTowers[randomIndex],
                                currentBot.transform.position + _botTowers[randomIndex].transform.position,
                                _botTowers[randomIndex].transform.rotation);
        tower.transform.SetParent(currentBot.transform);
        tower.transform.SetAsFirstSibling();
        randomIndex = Random.Range(0, _botChassies.Length);
        var chassie = Instantiate(_botChassies[randomIndex], currentBot.transform.position, _botChassies[randomIndex].transform.rotation);
        chassie.transform.SetParent(currentBot.transform);
        chassie.transform.SetAsFirstSibling();
    }
}
