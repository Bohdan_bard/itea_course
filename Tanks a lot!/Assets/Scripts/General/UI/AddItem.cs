﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddItem : MonoBehaviour
{
    
    [SerializeField]
    private GameObject _buttonTemplate = null;

    private Color32 _allyColor = new Color32(0,20,255,255); 
    private Color32 _enemyColor = new Color32(255,54,0,255); 
   

    public void AddItemToContent(string killer, string killed)
    {


        GameObject button = Instantiate(_buttonTemplate) as GameObject;
        button.SetActive(true);
        for (int i = 0; i < BotNames.EnemiesNames.Length; i++)
        {
            if (killer == BotNames.EnemiesNames[i])
            {
                button.GetComponent<Image>().color = _enemyColor;
                break;
            }
            button.GetComponent<Image>().color = _allyColor;
        }
     
        button.transform.SetParent(_buttonTemplate.transform.parent, false);
        button.transform.SetAsFirstSibling();
        button.GetComponent<KillInfoScript>().SetText(killer, killed);
    }
        
}
