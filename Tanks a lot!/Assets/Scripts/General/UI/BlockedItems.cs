﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using TMPro;
using UnityEngine.UI;

public class BlockedItems : MonoBehaviour
{
    private const int PointsToUnlock = 100;

    [SerializeField]
    private GameObject[] _unlockButtons= null;
    [SerializeField]
    private TextMeshProUGUI _pointsText = null;
    [SerializeField]
    private GameObject _submitScreen = null;
    [SerializeField]
    private GameObject _denyScreen = null;
    [SerializeField]
    private Button _submitButton = null;
    [SerializeField]
    private Button _denyButton = null;

    private string _savePath;
    private List<bool> _isBlocked;
    private int _points;
    private List<Button> _lockedButtons;
    private GameObject _buttonToUnlock = null;

    void Awake()
    {
        _savePath = Path.Combine(Application.persistentDataPath, "saveFile.txt");
    
    }

    private void Start()
    {
       
        _isBlocked = new List<bool>();
        _lockedButtons = new List<Button>();
        for (int i = 0; i < _unlockButtons.Length; i++)
        {
          var  index = i;
           _unlockButtons[i].GetComponent<Button>().onClick.AddListener(delegate { Unlock(_unlockButtons[index]); });
        }
        _points = PlayerPrefs.GetInt("Points", 19);
       
        _pointsText.text = $"Points: {_points}";
        if (File.Exists(_savePath))
        {
            Load();
            for (int i = 0; i < _unlockButtons.Length; i++)
            {
                _unlockButtons[i].SetActive(_isBlocked[i]) ;
            }

        }
        else {
            for (int i = 0; i < _unlockButtons.Length; i++)
            {
               
                _isBlocked.Add(_unlockButtons[i].activeInHierarchy);
            }
        }
        _submitButton.onClick.AddListener(SubmitUnlocking);
        _denyButton.onClick.AddListener(Deny);
    }

  public  void Save()
    {
        using (
           var writer = new BinaryWriter(File.Open(_savePath, FileMode.Create))
    )
        {
            for (int i = 0; i < _unlockButtons.Length; i++)
            {
                writer.Write(_unlockButtons[i].activeInHierarchy);
            }
        }
    }

  private  void Load()
    {
        using (
            var reader = new BinaryReader(File.Open(_savePath, FileMode.Open))
        )
        {
            for (int i = 0; i < _unlockButtons.Length; i++)
            {
               _isBlocked.Add( reader.ReadBoolean());
            }
              
        }
    }

    private void Unlock(GameObject buttonToUnlock)
    {
        if (_points >= PointsToUnlock)
        {
            _buttonToUnlock = buttonToUnlock;
            _submitScreen.SetActive(true);
        }
        else {
            _denyScreen.SetActive(true);
        }
    }

    private void SubmitUnlocking()
    {
        _points -= PointsToUnlock;
        _pointsText.text = $"Points: {_points}";
        PlayerPrefs.SetInt("Points", _points);
     
        _submitScreen.SetActive(false);
        _buttonToUnlock.SetActive(false);
        Save();
        _buttonToUnlock = null;
    }

    private void Deny()
    {
        _submitScreen.SetActive(false);
        _buttonToUnlock = null;
    }
}
