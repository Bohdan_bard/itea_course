﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public enum ChassiesType
{
ORIGINAL,
LIGHT,
HARD
}

public enum TowersType
{
CANNON,
MACHINEGUN,
FLAMETHROWER
}

public class ChoosePartScript : MonoBehaviour
{
    [SerializeField]
    private Button[] _chassiesButtons = null;
    [SerializeField]
    private Button[] _towerButtons = null;
    [SerializeField]
    private Button _startButton = null;
    [SerializeField]
    private GameObject[] _chassiesModels = null;
    [SerializeField]
    private GameObject[] _towerModels = null;

    private bool _isChassiesChoosen = false;
    private bool _isTowerChoosen = false;

    private Color _activeColor = new Color32() { r = 255, g = 192, b = 67, a = 255 };

    void Start()
    {
        for (int i = 0; i < _chassiesButtons.Length; i++)
        {
            var index = i;
            _chassiesButtons[i].onClick.AddListener(delegate { ChassiesChoice((ChassiesType)index); });
        }
         
        for (int i = 0; i < _chassiesButtons.Length; i++)
        {
            var index = i;
            _towerButtons[i].onClick.AddListener(delegate { TowerChoice((TowersType)index); });
        }

        _startButton.onClick.AddListener(StartGame);
    }

    private void Update()
    {
        if (_isChassiesChoosen && _isTowerChoosen)
        {
            _startButton.interactable = true;
        }
    }

    private void ChassiesChoice(ChassiesType type)
    {
        SetActiveButton((int)type, _chassiesButtons, _chassiesModels);
        PlayerPrefs.SetInt("ChassiesType", (int)type);
        _isChassiesChoosen = true;
    }

    private void TowerChoice(TowersType type)
    {
        SetActiveButton((int)type, _towerButtons, _towerModels);
        PlayerPrefs.SetInt("TowerType", (int)type);
        _isTowerChoosen = true;
    }

    private void StartGame()
    {
        GetComponent<BlockedItems>().Save();
        SceneManager.LoadScene("MainScene");
    }

    public void SetActiveButton(int index, Button[] buttons, GameObject[] models)
    {
        for (int i = 0; i < buttons.Length; i++)
        {
            if (i == index)
            {
                buttons[i].GetComponent<Image>().color = _activeColor;
                models[i].SetActive(true);
            }
            else {
                buttons[i].GetComponent<Image>().color = Color.white;
                models[i].SetActive(false);
            }
        }
    }
}
