﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameUIController : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI _timerText = null;
    [SerializeField]
    private TextMeshProUGUI _allyPointsText = null;
    [SerializeField]
    private TextMeshProUGUI _enemyPointsText = null;

    private int _roundTime;
    private LevelController _levelController;

    void Start()
    {
        _levelController = GetComponent<LevelController>();
        _roundTime = (int)_levelController.RoundTime;
        InvokeRepeating("UpdateTimer", 1f,1f);
        _levelController.SendAllyPoints += UpdateAllyPoints;
        _levelController.SendEnemyPoints += UpdateEnemyPoints;

        _timerText.text = $"{_roundTime}";
        UpdateAllyPoints(0);
        UpdateEnemyPoints(0);
    }

    private void UpdateTimer()
    {
        if (_levelController.IsGameOn)
        {
            _roundTime--;
            _timerText.text = $"{_roundTime}";
        }
    }

    private void UpdateAllyPoints(int points)
    {
        _allyPointsText.text = $"{points}";
    }

    private void UpdateEnemyPoints(int points)
    {
        _enemyPointsText.text = $"{points}";
    }
}
