﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class KillInfoScript : MonoBehaviour
{
    private TextMeshProUGUI _textField;
    
    void Start()
    {
        
        Invoke("Destroy", 4f);
    }

    public void SetText(string killerName, string killedName)
    {
        
        _textField = transform.GetChild(0).GetComponent<TextMeshProUGUI>();
        _textField.text = $"{killerName} killed {killedName}";
    }

    private void Destroy()
    {
        Destroy(gameObject);
    }
}
