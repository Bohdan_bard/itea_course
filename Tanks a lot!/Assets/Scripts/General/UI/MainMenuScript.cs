﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    [SerializeField]
    private Button _startButton = null;
    [SerializeField]
    private GameObject _chooseMenu = null;
 
    void Start()
    {
        _startButton.onClick.AddListener(StartGame);
    }

    private void StartGame()
    {
        _chooseMenu.SetActive(true);
        _startButton.transform.parent.gameObject.SetActive(false);
    }
}
