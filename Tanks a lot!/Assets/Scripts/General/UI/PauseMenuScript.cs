﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PauseMenuScript : MonoBehaviour
{
    [SerializeField]
    private Button _exitButton = null;
 
    private void OnEnable()
    {
        Time.timeScale = 0f;
    }
   
    void Start()
    {
        _exitButton.onClick.AddListener(Exit);
    }

    private void Exit()
    {
        SceneManager.LoadScene("MainMenu");
    }

    private void OnDisable()
    {
        Time.timeScale = 1f;
    }
}
