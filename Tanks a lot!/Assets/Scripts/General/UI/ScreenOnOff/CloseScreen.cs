﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CloseScreen : MonoBehaviour
{
    [SerializeField]
    private GameObject _screen = null;
    [SerializeField]
    private Button _button = null;

 
    void Start()
    {
        _button.onClick.AddListener(Close);
    }

    private void Close()
    {
        _screen.SetActive(false);
    }
}
