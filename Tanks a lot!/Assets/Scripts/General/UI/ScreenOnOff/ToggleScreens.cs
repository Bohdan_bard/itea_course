﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleScreens : MonoBehaviour
{
    [SerializeField]
    private GameObject _screenToTurnOff = null;
    [SerializeField]
    private GameObject _screenToTurnOn = null;
    [SerializeField]
    private Button _button = null;

    void Start()
    {
        _button.onClick.AddListener(Toggle);
    }

    private void Toggle()
    {
        _screenToTurnOn.SetActive(true);
        _screenToTurnOff.SetActive(false);
    }
}
