﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void RecycleAction(GameObject recycledObject);

public  class ObjectPool : MonoBehaviour
{
    
    private Dictionary<int, List<GameObject>> _poolsDictionary = new Dictionary<int, List<GameObject>>();

    public  GameObject ReturnObject(LayerMask layer)
    {
        if (_poolsDictionary.ContainsKey(layer))
        {

            if (_poolsDictionary[layer].Count > 0)
            {
                foreach (GameObject gameObject in _poolsDictionary[layer])
                {
                    if (!gameObject.activeInHierarchy)
                    {
                        return gameObject;
                    }
                }

                return null;
            }
            else
            {
                return null;
            }
        }

        else
        {
            _poolsDictionary.Add(layer, new List<GameObject>());          
            return null;
        }
        
    }

    public void AddToList(GameObject objectToAdd, LayerMask layer)
    {       
        _poolsDictionary[layer].Add(objectToAdd);
    }
}
