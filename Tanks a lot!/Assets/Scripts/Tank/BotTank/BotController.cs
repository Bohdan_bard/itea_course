﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public delegate void SendNames(string killer, string killed);

public class BotController : TankController
{
    public event SendNames SendNamesEvent;

    #region Fields

    [SerializeField]
    private Transform _hudCanvas = null;
    [SerializeField]
    private GameObject _tankExplosion = null;
    [SerializeField]
    private int _ultimateStrikeReloadTime = default;

    private NavMeshAgent _agent = null;
    private float _timeForRespawn = 5f;
    private float _timer;
    private Vector3 _staticPosition = default;
    private Quaternion _staticRotation = default;
    private Transform _chassies = null;
    private Transform _tower = null;
    private ObjectPool _pool;
    private LevelController _levelController;
    private int _health;
    private int _ammo;
    private BotTowerRotation _botTower;
    private Vector3 _startPosition;
    private bool _isEnemy;
    private Material _material;
    private TankHealth _tankHealth;
    private bool _isInactive = false;

    #endregion

    #region Properties

    public ObjectPool Pool { set { _pool = value; } }
    public LevelController LevelController { set { _levelController = value; } }
    public int Health { get { return _health; } }
    public int Ammo { get { return _ammo; } }
    public NavMeshAgent Agent { get { return _agent; } }
    public Transform Target { get; set; }
    public Vector3 StartPosition { get { return _startPosition; } }
    public bool IsEnemy { get { return _isEnemy; } set { _isEnemy = value; } }
    public Material Material { set { _material = value; } }
    public LayerMask EnemyLayer { get; set; }
    public int BulletReactLayer { get; set; }
    public int TeamLayer { get; set; }
    public string Name { get; set; }

    #endregion

    #region Methods
    private void Start()
    {
        _staticPosition = transform.position;
        _staticRotation = transform.rotation;
        _startPosition = transform.position;
        SetChassieStartParametrs();
        SetTowerStartParametrs();

        Target = _levelController.ReturnTarget(_isEnemy).GetChild(0).transform;

        GetComponent<TankUIController>().SetName(Name);
    }

    void Update()
    {
        if (_isInactive)
        {
            RespawnTimer();
        }
        if (_levelController.IsGameOn)
        {
            FindTarget();
        }
            _hudCanvas.position = _chassies.position;
        _tower.position = MoveTower(_chassies.position, _tower.position);

        if (transform.rotation != _staticRotation)
        {
            transform.position = _staticPosition;
            _staticRotation = transform.rotation;
        }
        else
        {        
            _staticPosition = transform.position;
        }      
    }

    private void SaveHealth(int health)
    {
        _health = health;
    }

    private void SaveAmmo(int ammo)
    {
        _ammo = ammo;
    }

    private void FindTarget()
    {
            if (Target != null)
            {
                if (!Target.gameObject.activeInHierarchy)
                {
                    if (_levelController.ReturnTarget(_isEnemy) == null)
                    {
                        Target = null;
                    }
                    else
                    {
                        Target = _levelController.ReturnTarget(_isEnemy).GetChild(0).transform;
                    }
                }
            }
            else
            {
                if (_levelController.ReturnTarget(_isEnemy) != null)
                {
                    Target = _levelController.ReturnTarget(_isEnemy).GetChild(0).transform;
                }
            }    
    }

    public override void TankDeath(int health)
    {
        if (health <= 0)
        {
            InitExplosion.Explode(_pool, _tankExplosion, _chassies.position);
            if (SendNamesEvent != null)
            {
                SendNamesEvent(_tankHealth.Name, Name);
            }
            _isInactive = true;
        }
        base.TankDeath(health);
    }

    private void RespawnTimer()
    {
            _timer += Time.deltaTime;
            if (_timer > _timeForRespawn)
            {
                _chassies.position = _startPosition;
                _chassies.gameObject.SetActive(true);
                _tower.gameObject.SetActive(true);
                _hudCanvas.gameObject.SetActive(true);
                _timer = 0;
                OnBecomeActive();
                _isInactive = false;
            }
    }

    private void SetChassieStartParametrs()
    {
        _chassies = GetChassies();

        while (!_chassies.GetComponent<TankHealth>())
        {
            _chassies = GetChassies();
        }
        _tankHealth = _chassies.GetComponent<TankHealth>();
        _tankHealth.SendHealth += GetComponent<TankUIController>().UpdateHealth;
        _tankHealth.SendHealth += TankDeath;
        _tankHealth.SendHealth += SaveHealth;
        _chassies.GetComponent<MeshRenderer>().material = _material;
        _chassies.gameObject.layer = TeamLayer;
        _agent = _chassies.GetComponent<NavMeshAgent>();
       
        GetComponent<FiniteStateMachine>().enabled = true;
    }

    private void SetTowerStartParametrs()
    {
        _tower = GetTower();
        while (!_tower.GetComponent<Shooting>())
        {
            _tower = GetTower();
        }
        var shooting = _tower.GetComponent<Shooting>();
        shooting.SendBulletsCount += GetComponent<TankUIController>().UpdateAmmo;
        shooting.SendBulletsCount += SaveAmmo;
        shooting.ObjectPool = _pool;
        shooting.EnemyTankLayer = BulletReactLayer;
        shooting.ShooterName = Name;

        _botTower = _tower.GetComponent<BotTowerRotation>();
        _botTower.Layer = EnemyLayer;
        _botTower.UltimateStrikeReloadTime = _ultimateStrikeReloadTime;

        _tower.GetComponent<MeshRenderer>().material = _material;

        _tower.gameObject.layer = TeamLayer;
    }

    #endregion
}
