﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BotTowerRotation : Rotation
{
    private const int WallLayer = 12;
    private const int ShotsForUltimateStrike = 3;

    [SerializeField]
    private float _rotationSpeed = 100f;
    [SerializeField]
    private float _shootingPeriod = default;

    private LayerMask _layer = default;
    private float _timer = 0f;
    private Vector3 _target = default;
    private bool _enemyFound = false;
    private Shooting _botShooting = null;
    private int _ultimateStrikeReloadTime;
    private float _ultimateStrikeReloadTimer;
    private int _shotCount=0;

    public int UltimateStrikeReloadTime { set { _ultimateStrikeReloadTime = value; } }
    public LayerMask Layer { set { _layer = value; } }

    private void Start()
    {
        _botShooting = GetComponent<Shooting>();
        _ultimateStrikeReloadTimer = _ultimateStrikeReloadTime;
    }

    void Update()
    {
        CheckForEnemy();

        if (_enemyFound)
        {
            _target -= transform.position;
            Rotate(_target, _rotationSpeed);
        }

        _timer += Time.deltaTime;
        if (_enemyFound && _timer >=_shootingPeriod)
        {
            int bitWallLayer = 1 << WallLayer;
            RaycastHit hit;
         
            if (!Physics.Raycast(transform.position, transform.TransformDirection(Vector3.forward), out hit, Vector3.Distance(transform.position, _target), bitWallLayer))
            {
                EnemyShoot();
                _timer = 0f;
            }
          
        }
        if (_ultimateStrikeReloadTimer < _ultimateStrikeReloadTime)
        {
            _ultimateStrikeReloadTimer += Time.deltaTime;
        }

        if (_shotCount >= ShotsForUltimateStrike)
        {
            EnemyUltimateStrike();
        }
    }

    public override void CheckForEnemy()
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, 10f, _layer);
        switch (hitColliders.Length)
        {
            case 0: _enemyFound = false; break;
            case 1:
                _enemyFound = true;
                _target = hitColliders[0].transform.position;
                break;
            default:
                Vector3 nearestTarget = default;
                float nearestDistance = float.MaxValue;
                float distance;

                for (int i = 0; i < hitColliders.Length; i++)
                {
                    distance = Vector3.Distance(transform.position, hitColliders[i].transform.position);
                    if (distance < nearestDistance)
                    {
                        nearestDistance = distance;
                        nearestTarget = hitColliders[i].transform.position;
                    }
                }
                _target = nearestTarget;
                break;
        }
    }

    private void EnemyShoot()
    {
        _shotCount++;
        _botShooting.Shoot();
    }

    private void EnemyUltimateStrike()
    {
        if (_ultimateStrikeReloadTimer >= _ultimateStrikeReloadTime)
        {
            _botShooting.UltimateStrike();
            _ultimateStrikeReloadTimer = 0;
        }
    }
}
