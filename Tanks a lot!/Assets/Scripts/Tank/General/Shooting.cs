﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    public event SendInfoAction SendBulletsCount;

    private const int MaxBulletCount = 5;

    [SerializeField]
    private GameObject _bullet = null;
    [SerializeField]
    private GameObject _hardBullet = null;
    [SerializeField]
    private Transform _bulletSpawnPoint = null; 
    [SerializeField]
    private float _timeToReload = default;

    private int _enemyTankLayer = default;
    private float _reloadTimer = 0; 
    private int _bulletCount = 5;
    private ObjectPool _objectPool = null;
    private AudioSource _audioSource;

    public ObjectPool ObjectPool { get { return _objectPool; } set { _objectPool = value; } }
    public int EnemyTankLayer { get { return _enemyTankLayer; } set { _enemyTankLayer = value; } }
    public string ShooterName { get; set; }

    private void OnEnable()
    {
        SetStartingAmmo();
    }

    private void Start()
    {
        SetStartingAmmo();
        _audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        _reloadTimer += Time.deltaTime;
        if (_reloadTimer >= _timeToReload)
        {
            Reload();
            _reloadTimer = 0;
        }
    }

    public void Shoot()
    {
        _audioSource.Play();
        if (_bulletCount > 0)
        {
            CreateBullet(_bullet);
            _bulletCount--;
            if (SendBulletsCount != null)
            {
                SendBulletsCount(_bulletCount);
            }
            _reloadTimer = 0;
        }
    }

    private void Reload()
    {
        if (_bulletCount < 5)
        {
            _bulletCount++;
        }

        if (SendBulletsCount != null)
        {
            SendBulletsCount(_bulletCount);
        }
    }

    private void SetStartingAmmo()
    {
        _bulletCount = MaxBulletCount;
        if (SendBulletsCount != null)
        {
            SendBulletsCount(_bulletCount);
        }
    }

    public void UltimateStrike()
    {
        CreateBullet(_hardBullet);
    }

    public void CreateBullet(GameObject prefab)
    {
        var bullet = _objectPool.ReturnObject(prefab.layer);
        if (bullet == null)
        {
            bullet = Instantiate(prefab, _bulletSpawnPoint.position, _bulletSpawnPoint.rotation, _objectPool.transform);
            var ammoController = bullet.transform.GetChild(0).GetComponent<AmmoController>();
            ammoController.Pool = _objectPool;
            SetAmmoParametrs(ammoController);
            _objectPool.AddToList(bullet, prefab.layer);
        }
        else
        {
            bullet.transform.position = _bulletSpawnPoint.position;
            bullet.transform.GetChild(0).position = _bulletSpawnPoint.position;
            bullet.transform.rotation = _bulletSpawnPoint.rotation;
            bullet.SetActive(true);
        }
    }

    private void SetAmmoParametrs(AmmoController ammoController)
    {
        ammoController.ShooterName = ShooterName;
        ammoController.EnemyLayer = _enemyTankLayer;
    }
}
