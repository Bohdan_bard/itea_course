﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TankController : MonoBehaviour
{
    public delegate void SendGameObjectAction(GameObject tank);

    public event SendGameObjectAction RemoveFromActiveListEvent;
    public event SendGameObjectAction AddToActiveListEvent;
    

    public virtual Transform GetChassies()
    {
        return transform.GetChild(0);
    }

    public virtual Transform GetTower()
    {
        return transform.GetChild(1);
    }

    public virtual void TankDeath(int health)
    {
        if (health <= 0)
        {
            transform.GetChild(0).gameObject.SetActive(false);
            transform.GetChild(1).gameObject.SetActive(false);
            transform.GetChild(2).gameObject.SetActive(false);
            RemoveFromActiveListEvent(gameObject);
        }
    }

    public virtual Vector3 MoveTower(Vector3 chassiesPosition, Vector3 towerPosition)
    {
        return  new Vector3(chassiesPosition.x, towerPosition.y, chassiesPosition.z);
    }

    public virtual void OnBecomeActive()
    {
        if(AddToActiveListEvent!=null)
        AddToActiveListEvent(gameObject);
    }
}

  
