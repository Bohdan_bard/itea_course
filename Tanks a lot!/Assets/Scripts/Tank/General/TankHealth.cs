﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void SendInfoAction(int value);

public class TankHealth : MonoBehaviour
{
    public event SendInfoAction SendHealth;

    [SerializeField]
    private float _healthRecoveryTime = default;
    [SerializeField]
    private int _maxHealth = 100;

    private float _recoveryTimer = 0;
    private int _health;
    private string _lastShooter;

    public string Name { get { return _lastShooter; } }

    private void OnEnable()
    {
        SetStartingHealth();
    }

    private void Start()
    {
        SetStartingHealth();
    }

    private void Update()
    {
        _recoveryTimer += Time.deltaTime;
        if (_recoveryTimer >= _healthRecoveryTime)
        {
            AddHealth();
        }
    }

    public void DecreaseHealth(int damage, string lastShooter)
    {
        _lastShooter = lastShooter;
        _recoveryTimer = 0;
        _health -= damage;
        if (SendHealth != null)
        {
            SendHealth(_health);
        }
    }

    private void AddHealth()
    {
        if (_health < _maxHealth)
        {
            _health++;
            if (SendHealth != null)
            {
                SendHealth(_health);
            }
        }
    }

    private void SetStartingHealth()
    {
        _health = _maxHealth;
        if (SendHealth != null)
        {
            SendHealth(_health); 
        }
    }
}
