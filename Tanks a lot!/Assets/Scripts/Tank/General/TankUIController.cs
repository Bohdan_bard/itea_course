﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TankUIController : MonoBehaviour
{
    [SerializeField]
    private Slider _healthBar = null;
    [SerializeField]
    private Slider _ammoBar = null;
    [SerializeField]
    private TextMeshProUGUI _nameText = null;

    private bool _firstEnter = true;

    public void UpdateHealth(int health)
    {
        if(_firstEnter)
        {
            _healthBar.maxValue = health;
            _firstEnter = false;
        }
        _healthBar.value = health;
    }

    public void UpdateAmmo(int ammo)
    {
        _ammoBar.value = ammo;
    }

    public void SetName(string name)
    {
        _nameText.text = name;
    }
}
