﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class JoystickController : MonoBehaviour, IPointerUpHandler
{
    public delegate void PointerUpAction();

    public  event PointerUpAction PointerUpEvent;

    public void OnPointerUp(PointerEventData eventData)
    {
            if (PointerUpEvent != null)
            {
                PointerUpEvent();  
            }
    }
}
