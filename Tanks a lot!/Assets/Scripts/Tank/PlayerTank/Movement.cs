﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{
    [SerializeField]
    private float _maxSpeed = 1f;
    [SerializeField]
    private float _force = 2000;
    [SerializeField]
    private AudioClip _idleSound = null;
    [SerializeField]
    private AudioClip _movementSound = null;
    [SerializeField]
    private float _pitchRange = 0.2f;

    private AudioSource _movementAudio;
    private Joystick _movementJoystick;
    private Rigidbody _rigidbody = null;
    private bool _isMooving = true;
    private float _originalPitch;

    public Joystick MovementJoystick
    {
        set => _movementJoystick = value;
    }

    public bool IsMooving
    {
      set => _isMooving = value;
    }
   
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _movementAudio = GetComponent<AudioSource>();
        _originalPitch = _movementAudio.pitch;
    }
  
    void Update()
    {
        if (_isMooving)
        {
            MoveObject();
            EngineAudio();
        }
    }

    private void MoveObject()
    {
        if ((_movementJoystick.Horizontal !=0)  && (_movementJoystick.Vertical !=0))
        {
            if (_rigidbody.velocity.magnitude >= _maxSpeed)
            {
                return;
            }
            _rigidbody.AddForce(transform.forward * _force);            
        }
    }

    private void EngineAudio()
    {
        if (Mathf.Abs(_movementJoystick.Horizontal) < 0.1f && Mathf.Abs(_movementJoystick.Vertical)< 0.1f)
        {
            if (_movementAudio.clip == _movementSound)
            {
                _movementAudio.clip = _idleSound;
                _movementAudio.pitch = Random.Range(_originalPitch - _pitchRange, _originalPitch + _pitchRange);
                _movementAudio.Play();
            }
        }
        else
        {
            if (_movementAudio.clip == _idleSound)
            {
                _movementAudio.clip = _movementSound;
                _movementAudio.pitch = Random.Range(_originalPitch - _pitchRange, _originalPitch + _pitchRange);
                _movementAudio.Play();
            }
        }
    }
}
