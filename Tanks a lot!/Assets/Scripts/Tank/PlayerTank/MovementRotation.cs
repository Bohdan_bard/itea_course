﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementRotation : Rotation
{
    [SerializeField]
    private float _rotationSpeed = 100f;

    private Movement _chassiesMovement;
    private Joystick _joystick = null;
    public Joystick Joystick { set { _joystick = value; } }

    private void Start()
    {
        _chassiesMovement = GetComponent<Movement>();
    }
    
    void Update()
    {
        if (_joystick.Horizontal != 0 && _joystick.Vertical != 0)
        {
            Rotation();
        }
    }

    public override void Rotate(Vector3 targetDirection, float rotationSpeed)
    {
        base.Rotate(targetDirection, rotationSpeed);
    }

    private void Rotation()
    {
        Vector3 targetDirection = new Vector3(_joystick.Horizontal, 0f, _joystick.Vertical);

        Rotate(targetDirection, _rotationSpeed);

        float angle = Vector3.Angle(Vector3.forward, targetDirection);
        if (_joystick.Horizontal < 0)
        {
            angle =360-angle;
        }
     
        if (transform.rotation.eulerAngles.y <angle+3f && transform.rotation.eulerAngles.y>angle-3f)
        {
            _chassiesMovement.IsMooving = true;
        }
        else
        {
            _chassiesMovement.IsMooving = false;
        }
    }
}
