﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : TankController
{
    public event SendNames SendNamesEvent;

    #region Fields
    [SerializeField]
    private Transform _hudCanvas = null;
    [SerializeField]
    private float _minXCameraPosition = default;
    [SerializeField]
    private float _maxXCameraPosition=default;
    [SerializeField]
    private float _minYCameraPosition=default;
    [SerializeField]
    private float _maxYCameraPosition=default;
    [SerializeField]
    private GameObject _tankExplosion = null;
    [SerializeField]
    private int _ultimateStrikeReloadTime = default;
   

    private Slider _reloadSlider;
    private Vector3 _startPosition;
    private float _timeForRespawn = 5f;
    private float _timer;
    private Transform _chassies = null;
    private Transform _tower=null;
    private ObjectPool _pool;
    private Transform _camera;
    private Joystick _movementJoystick = null;
    private Joystick _shootingJoystick = null;
    private Joystick _ultimateJoystick = null;
    private TankHealth _tankHealth;
    private bool _isInactive = false;
    private float _ultimateAttackReloadTimer = 0;
    private Shooting _shooting;

    #endregion

    #region Properties

    public Joystick MovementJoystick { set { _movementJoystick = value; } }
    public Joystick ShootingJoystick { set { _shootingJoystick = value; } }
    public Joystick UltimateJoystick { set { _ultimateJoystick = value; } }
    public ObjectPool Pool { set { _pool = value; } }
    public Transform Camera { set { _camera = value; } }
    public string Name { get; set; }
    public Slider ReloadSlider { set { _reloadSlider = value; } }

    #endregion

    #region Methods
    void Start()
    {
        _startPosition = transform.position;
        _chassies =  GetChassies();
        while (!_chassies.GetComponent<Movement>())
        { _chassies = GetChassies(); }
        _chassies.GetComponent<Movement>().MovementJoystick = _chassies.GetComponent<MovementRotation>().Joystick = _movementJoystick;
        _tower = GetTower();
        while (!_tower.GetComponent<TowerRotation>())
        {
            _tower = GetTower();
        }
       
        _tower.GetComponent<TowerRotation>().Joystick = _shootingJoystick;
        _tower.GetComponent<TowerRotation>().UltimateJoyStick = _ultimateJoystick;

        _shootingJoystick.GetComponent<JoystickController>().PointerUpEvent += _tower.GetComponent<Shooting>().Shoot ;
        _ultimateJoystick.GetComponent<JoystickController>().PointerUpEvent += UltimateStrike;

        _tankHealth = _chassies.GetComponent<TankHealth>();
        _tankHealth.SendHealth += GetComponent<TankUIController>().UpdateHealth;
        _tankHealth.SendHealth += TankDeath;

        _shooting = _tower.GetComponent<Shooting>();
        _shooting.SendBulletsCount += GetComponent<TankUIController>().UpdateAmmo;
        _shooting.ObjectPool = _pool;
        _shooting.EnemyTankLayer = 11;
        _shooting.ShooterName = Name;

        GetComponent<TankUIController>().SetName(Name);

        _ultimateAttackReloadTimer = _ultimateStrikeReloadTime;
        _reloadSlider.maxValue = _ultimateStrikeReloadTime;
        _reloadSlider.value = _ultimateStrikeReloadTime;
    }

    private void Update()
    {
        if (_isInactive)
        {
            RespawnTimer();
        }
        _tower.position =   MoveTower(_chassies.position, _tower.position);
        _hudCanvas.position = _chassies.position;
        _camera.position = new Vector3( Mathf.Clamp(_chassies.position.x, _minXCameraPosition,_maxXCameraPosition), _camera.position.y, 
                                        Mathf.Clamp(_chassies.position.z-15f,_minYCameraPosition,_maxYCameraPosition));

        if (_ultimateAttackReloadTimer < _ultimateStrikeReloadTime)
        {
            _ultimateAttackReloadTimer += Time.deltaTime;
        }

        _reloadSlider.value = _ultimateAttackReloadTimer;
    }

    public override void TankDeath(int health)
    {
        if (health <= 0)
        {
            InitExplosion.Explode(_pool, _tankExplosion, _chassies.position);
          
            if (SendNamesEvent != null)
            {
                SendNamesEvent(_tankHealth.Name, Name);
            }
            _isInactive = true;
        }
        base.TankDeath(health);
    }

    private void RespawnTimer()
    {
            _timer += Time.deltaTime;
            if (_timer > _timeForRespawn)
            {
                _chassies.position = _startPosition;
                _chassies.gameObject.SetActive(true);
                _tower.gameObject.SetActive(true);
                _hudCanvas.gameObject.SetActive(true);
                _timer = 0;
                OnBecomeActive();
                _isInactive = false;
            }
    }

    private void UltimateStrike()
    {
        if (_ultimateAttackReloadTimer >= _ultimateStrikeReloadTime)
        {
            _shooting.UltimateStrike();
            _ultimateAttackReloadTimer = 0;
        }
    }
    #endregion
}
