﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerRotation : Rotation
{
    [SerializeField]
    private float _rotationSpeed = 100f;
    [SerializeField]
    private LayerMask _layer = default;

    private Vector3 _target = default;
    private bool _enemyFound = false;
    private Joystick _joystick = null;
    private Joystick _ultimateJoystick = null;

    public Joystick Joystick { set { _joystick = value; } }
    public Joystick UltimateJoyStick { set { _ultimateJoystick = value; } }

    void Update()
    {

        if (_joystick.Horizontal > 0.5f || _joystick.Vertical > 0.5f || _joystick.Horizontal<-0.5f || _joystick.Vertical<-0.5f)
        {
            Rotation(_joystick.Horizontal,_joystick.Vertical);
            return;
        }
        if (_ultimateJoystick.Horizontal > 0.5f || _ultimateJoystick.Vertical > 0.5f || _ultimateJoystick.Horizontal < -0.5f || _ultimateJoystick.Vertical < -0.5f)
        {
            Rotation(_ultimateJoystick.Horizontal, _ultimateJoystick.Vertical);
            return;
        }

        CheckForEnemy();

        if (_enemyFound)
        {
            _target -= transform.position;
            Rotate(_target, _rotationSpeed);
        }
    }

    public override void Rotate(Vector3 targetDirection, float rotationSpeed)
    {
        base.Rotate(targetDirection, rotationSpeed);
    }

    private void Rotation(float horizontal, float vertical)
    {
        Vector3 targetDirection = new Vector3(horizontal, 0f, vertical);
        Rotate(targetDirection, _rotationSpeed);
    }

    public override void CheckForEnemy()
    {
        Collider[] hitColliders = Physics.OverlapSphere(transform.position, 10f, _layer);
        switch (hitColliders.Length)
        {
            case 0: _enemyFound = false; break;
            case 1:
                _enemyFound = true;
                _target = hitColliders[0].transform.position;
                break;
            default:
                Vector3 nearestTarget = default;
                float nearestDistance = float.MaxValue;
                float distance;

                for (int i = 0; i < hitColliders.Length; i++)
                {
                    distance = Vector3.Distance(transform.position, hitColliders[i].transform.position);
                    if (distance < nearestDistance)
                    {
                        nearestDistance = distance;
                        nearestTarget = hitColliders[i].transform.position;
                    }
                }
                _target = nearestTarget;
                break;
        }
    }
}
